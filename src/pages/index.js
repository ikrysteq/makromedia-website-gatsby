import React from 'react'
import Layout from '../components/Layout'
import SEO from '../components/seo'
import GlobalStyle from '../theme/GlobalStyle'
import HomeBannerSection from '../components/Home/HomeBannerSection'
import HomeWhatWeDoSection from '../components/Home/HomeWhatWeDoSection'
import HomeStatisticsSection from '../components/Home/HomeStatisticsSection'
import HomeBenefitsSection from '../components/Home/HomeBenefitsSection'
import HomeWhyChooseUsSection from '../components/Home/HomeWhyChooseUsSection'
import HomeWeWorkForSection from '../components/Home/HomeWeWorkForSection'
import HomeGoogleServicesSection from '../components/Home/HomeGoogleServicesSection'
import HomeTrustedUsSection from '../components/Home/HomeTrustedUsSection'
import HomeLocationSection from '../components/Home/HomeLocationSection'

const IndexPage = () => (
  <>
    <GlobalStyle />
    <Layout>
      <SEO
        title="Marketing internetowy"
        keywords={[`gatsby`, `application`, `react`]}
      />
      <HomeBannerSection />
      <HomeWhatWeDoSection />
      <HomeStatisticsSection />
      <HomeBenefitsSection />
      <HomeWhyChooseUsSection />
      <HomeWeWorkForSection />
      <HomeGoogleServicesSection />
      <HomeTrustedUsSection />
      <HomeLocationSection />
    </Layout>
  </>
)

export default IndexPage
