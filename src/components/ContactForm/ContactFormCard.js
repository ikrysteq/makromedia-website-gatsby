import { graphql, StaticQuery } from 'gatsby'
import React from 'react'
import axios from 'axios'
import styled from 'styled-components'
import Swal from 'sweetalert2'
import config from '../../config/variables'
import { colors } from '../../utils/colors'
import ContactFormInput from './ContactFormInput'
import withReactContent from 'sweetalert2-react-content'
class ContactFormCard extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      companyName: '',
      phoneNumber: '',
      companyNameValid: false,
      phoneNumberValid: false,
      formErrors: { companyName: '', phoneNumber: '' },
      formValid: false,
      loading: false,
    }

    this.handleInputChange = this.handleInputChange.bind(this)
    this.validateField = this.validateField.bind(this)
    this.sendForm = this.sendForm.bind(this)
  }

  handleInputChange(event) {
    const target = event.target
    const value = target.value
    const name = target.name

    this.setState(
      {
        [name]: value,
      },
      () => {
        this.validateField(name, value)
      }
    )
  }

  validateField(fieldName, value) {
    let fieldValidationErrors = this.state.formErrors
    let companyNameValid = this.state.companyNameValid
    let phoneNumberValid = this.state.phoneNumberValid
    const regexName = /[a-z0-9.+()\\/-]/gi
    /* eslint-disable-next-line */
    const regexPhoneNumber = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/g

    switch (fieldName) {
      case 'companyName':
        companyNameValid = regexName.test(value) && value.length > 0
        fieldValidationErrors.companyName = companyNameValid
          ? ''
          : ' is invalid'
        break
      case 'phoneNumber':
        phoneNumberValid = regexPhoneNumber.test(value) && value.length > 0
        fieldValidationErrors.phoneNumber = phoneNumberValid
          ? ''
          : ' is invalid'
        break
      default:
        break
    }

    this.setState({
      formErrors: fieldValidationErrors,
      companyNameValid,
      phoneNumberValid,
      formValid: companyNameValid && phoneNumberValid,
    })
  }

  sendForm() {
    const MySwal = withReactContent(Swal),
      me = this

    me.setState({
      loading: true,
    })

    axios
      .post(config.sendContactFormApiEndpoint, {
        companyName: this.state.companyName,
        phoneNumber: this.state.phoneNumber,
      })
      .then(response => {
        me.setState({
          loading: false,
        })
        MySwal.fire({
          type: 'success',
          title: <p>Wysłano</p>,
          timer: 10000,
          confirmButtonColor: colors.nav,
          cancelButtonColor: colors.brand,
          footer: (
            <span>
              &nbsp; Nasz doradca zadzwoni do Państwa <br />w ciągu 30 minut (pn
              - pt, 8.00 - 16.00)
            </span>
          ),
        })
      })
      .catch(function(error) {
        me.setState({
          loading: false,
        })
        MySwal.fire({
          type: 'error',
          title: <p>Wystąpił błąd</p>,
          timer: 10000,
          confirmButtonColor: colors.nav,
          cancelButtonColor: colors.brand,
          footer: <span>{String(error)}</span>,
        })
      })
  }

  render() {
    return (
      <StaticQuery
        query={componentQuery}
        render={props => (
          <StyledContainer className="quick-contact-container text-center">
            <Box className="box">
              <Title>Szybki kontakt</Title>
              <form>
                <ContactFormInput
                  id="companyName"
                  name="companyName"
                  placeholder="Imię i nazwisko / firma"
                  value={this.state.companyName}
                  onChange={this.handleInputChange}
                />
                <ContactFormInput
                  id="phoneNumber"
                  name="phoneNumber"
                  placeholder="Numer telefonu"
                  value={this.state.phoneNumber}
                  onChange={this.handleInputChange}
                />
                <button
                  type="button"
                  className="btn"
                  id="sendCredentialsButton"
                  disabled={!this.state.formValid || this.state.loading}
                  onClick={this.sendForm}
                >
                  {this.state.loading && (
                    <Loader src={props.loadingFile.publicURL} />
                  )}
                  {!this.state.loading && 'Wyślij zapytanie'}
                </button>
              </form>
              <BottomInfo>
                Nasz doradca zadzwoni do Państwa
                <br /> w ciągu 30 minut (pn - pt, 8.00 - 16.00)
              </BottomInfo>
            </Box>
          </StyledContainer>
        )}
      />
    )
  }
}

const StyledContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  text-align: center;
  padding: 0;
  height: 400px;
  background-color: ${({ theme }) => theme.colors.accent};

  @media ${({ theme }) => theme.media.tablet} {
    padding: 100px 0;
    padding-top: 10vh;
    height: 100%;
    background: none;
  }

  @media only screen and (min-height: 1000px) {
    padding-top: 100px;
    height: 1000px;
  }
`
const Box = styled.div`
  background-color: rgba(255, 255, 255, 0.32);
  border: 1px solid #eee;
  border-radius: 10px;
  max-width: 290px;
  height: 310px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  padding: 10px;

  p {
    color: #fff;
  }

  form {
    button {
      width: 187px;
      height: 40px;
      margin: 13px auto;
      border: none;
      border-radius: 10px;
      text-transform: uppercase;
      font-size: 13px;
      font-weight: 600;
      color: ${({ theme }) => theme.colors.white};
      background-color: ${({ theme }) => theme.colors.brand};
      cursor: pointer;
      outline: none;

      &:hover {
        background-color: #e52e2e;
      }

      &:active {
        background-color: #e84545;
      }

      &[disabled] {
        cursor: not-allowed;
        box-shadow: none;
        opacity: 0.65;
      }
    }
  }
`

const Title = styled.p`
  padding-top: 18px;
  margin: 0 0 10px;
  font-size: 23px;
  text-transform: uppercase;
  font-weight: 500;
  font-family: ${({ theme }) => theme.font.family.primary};
  user-select: none;
`
const Loader = styled.img`
  height: 40px;
`
const BottomInfo = styled.p`
  font-size: 13px;
  line-height: 14px;
  text-align: center;

  @media ${({ theme }) => theme.media.desktop} {
    font-size: inherit;
  }
`

const componentQuery = graphql`
  query {
    fileName: file(relativePath: { eq: "logo.png" }) {
      childImageSharp {
        fixed(height: 80) {
          ...GatsbyImageSharpFixed_withWebp_tracedSVG
        }
      }
    }
    loadingFile: file(relativePath: { eq: "icons/loading.svg" }) {
      publicURL
    }
  }
`

export default ContactFormCard
