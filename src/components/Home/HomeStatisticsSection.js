import React from 'react'
import styled from 'styled-components'
import { Row, Col } from 'react-styled-flexboxgrid'
import AppContainer from '../AppContainer'
import backgroundImage from '../../images/mountains.jpg'

const HomeStatisticsSection = () => (
  <Section>
    <AppContainer>
      <Row>
        <Info
          title="716"
          descriptionTop="Firm, które"
          descriptionBottom="nam zaufało"
        />
        <Info
          title="43140"
          descriptionTop="Pozycjonowanych"
          descriptionBottom="fraz kluczowych"
        />
        <Info
          title="13 lat"
          descriptionTop=" Efektów naszej"
          descriptionBottom="pracy"
        />
        <Info
          title="86%"
          descriptionTop="Przedłużonych"
          descriptionBottom="umów"
        />
      </Row>
    </AppContainer>
  </Section>
)

const Info = props => (
  <Column xs={12} sm={6} md={3}>
    <Title>{props.title}</Title>
    <Description>
      {props.descriptionTop}
      <br />
      {props.descriptionBottom}
    </Description>
  </Column>
)

const Section = styled.section`
  background-image: url(${backgroundImage});
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center center;
  height: 100%;
  padding: 0;
  font-family: ${({ theme }) => theme.font.family.secondary};
  font-weight: ${({ theme }) => theme.font.weight.thin};
  color: ${({ theme }) => theme.colors.white};
`

const Column = styled(Col)`
  height: 200px;
  padding-top: 16px;
  text-align: center;

  @media ${({ theme }) => theme.media.tablet} {
    height: 300px;
    padding-top: 70px;
  }
`

const Title = styled.h3`
  font-size: 42pt;
  color: ${({ theme }) => theme.colors.white};
`

const Description = styled.p`
  font-size: 14pt;
  margin-top: 20px;
`

export default HomeStatisticsSection
