import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Row, Col } from 'react-styled-flexboxgrid'
import SectionTitle from '../SectionTitle'
import AppContainer from '../AppContainer'
import hillsImage from '../../images/hills.png'
import IconManager from '../../images/icons/manager.svg'
import IconNewFile from '../../images/icons/new-file.svg'
import IconRecommended from '../../images/icons/recommended.svg'
import IconLineChart from '../../images/icons/line-chart.svg'

const HomeWhyChooseUsSection = () => (
  <Section>
    <SectionTitle>Dlaczego warto Nas wybrać?</SectionTitle>
    <Container>
      <Row>
        <Column xs={12} md={3}>
          <Figure
            title="Doradztwo"
            decription="Lorem ipsum dolor sit amet, consectetur adipiscing elit.. Phasellus lacinia tortor vel est maximus pretium. Sed vitae velit ut nibh"
            class="square-rounded"
          >
            <IconManager />
          </Figure>
        </Column>
        <Column xs={12} md={3}>
          <Figure
            title="Korzystna oferta"
            decription="Lorem ipsum dolor sit amet, consectetur adipiscing elit.. Phasellus lacinia tortor vel est maximus pretium. Sed vitae velit ut nibh"
            class="square-rounded"
          >
            <IconNewFile />
          </Figure>
        </Column>
        <Column xs={12} md={3}>
          <Figure
            title="Profesjonalizm"
            decription="Lorem ipsum dolor sit amet, consectetur adipiscing elit.. Phasellus lacinia tortor vel est maximus pretium. Sed vitae velit ut nibh"
            class="square-rounded"
          >
            <IconRecommended />
          </Figure>
        </Column>
        <Column xs={12} md={3}>
          <Figure
            title="Wieloletnie doświadczenie"
            decription="Lorem ipsum dolor sit amet, consectetur adipiscing elit.. Phasellus lacinia tortor vel est maximus pretium. Sed vitae velit ut nibh"
          >
            <IconLineChart />
          </Figure>
        </Column>
      </Row>
    </Container>
  </Section>
)

const Figure = ({ title, decription, children }) => (
  <>
    <SquareRounded>
      <SquareRoundedContent>{children}</SquareRoundedContent>
    </SquareRounded>
    <h2>{title}</h2>
    <p>{decription}</p>
  </>
)

const Section = styled.section`
  height: 100%;
  min-height: 100vh;
  margin: 70px auto;

  @media ${({ theme }) => theme.media.tablet} {
    background-image: url(${hillsImage});
    background-position: center bottom;
    background-repeat: no-repeat;
    background-size: cover;
    height: 650px;
    min-height: 650px;
    margin: 60px auto auto auto;
  }
`

const Container = styled(AppContainer)`
  min-height: 300px;
  padding-top: 0;

  h2 {
    margin-top: -12pt;
    text-transform: uppercase;
    font-size: 14pt;
    font-weight: ${({ theme }) => theme.font.weight.bold};
    color: ${({ theme }) => theme.colors.fontColor};
  }

  p {
    margin-top: 2pt;
    font-size: 9pt;
    line-height: initial;
  }
`

const Column = styled(Col)`
  display: flex;
  justify-content: center;
  flex-direction: column;
  padding-left: 10px;
  padding-right: 10px;
  text-align: center;

  @media ${({ theme }) => theme.media.tablet} {
    &:nth-child(2),
    &:nth-child(3) {
      margin-top: 60px;
    }
  }
`

const SquareRounded = styled.figure`
  border-radius: 20px;
  background: linear-gradient(to bottom right, #58d5f7, #2176a9);
  transform: rotate(45deg);
  position: relative;
  width: 115px;
  height: 115px;
  box-shadow: rgba(0, 0, 0, 0.13) 9px 0px 0;
  margin: 50px auto;
  display: flex;
  justify-content: center;
  align-items: center;
  overflow: hidden;
  transition: all 0.3s;

  &:hover {
    transform: translateY(-8px) rotate(45deg);
    box-shadow: rgba(0, 0, 0, 0.13) 14px 0px 0;
  }
`

const SquareRoundedContent = styled.div`
  transform: rotate(-45deg);

  svg {
    width: 65%;
  }
`

Figure.propTypes = {
  children: PropTypes.node.isRequired,
}

export default HomeWhyChooseUsSection
