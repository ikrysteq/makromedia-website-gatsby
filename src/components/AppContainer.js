// import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components'

class AppContainer extends React.Component {
  render() {
    return (
      <Container className={this.props.className}>
        {this.props.children}
      </Container>
    )
  }
}

const Container = styled.div`
  position: relative;
  width: auto;
  padding: 0;
  margin: 0 3%;

  @media ${({ theme }) => theme.media.tablet} {
    width: ${({ theme }) => theme.flexboxgrid.container.md}rem;
    max-width: ${({ theme }) => theme.flexboxgrid.container.md}rem;
    margin: 0 auto;
  }

  @media ${({ theme }) => theme.media.desktop} {
    width: ${({ theme }) => theme.flexboxgrid.container.lg}rem;
    max-width: ${({ theme }) => theme.flexboxgrid.container.lg}rem;
  }
`

export default AppContainer
