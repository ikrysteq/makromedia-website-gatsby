import React from 'react'
import styled from 'styled-components'
import AppGoogleMap from '../AppGoogleMap'
import { Grid, Row, Col } from 'react-styled-flexboxgrid'
import { FaPhone, FaEnvelope } from 'react-icons/fa'

const ContactSection = () => (
  <StyledSection>
    <Grid>
      <Row>
        <Column xs={12} md={6}>
          <MapWrapper>
            <AppGoogleMap />
          </MapWrapper>
        </Column>
        <Column xs={12} md={6}>
          <AddressContent>
            <AddressContainer />
          </AddressContent>
        </Column>
      </Row>
    </Grid>
  </StyledSection>
)

const AddressContainer = () => (
  <StyledAddress>
    <h3>Makromedia Sp. z o.o.</h3>
    <InfoText>
      ul. Dworcowa 81
      <br />
      85-009 Bydgoszcz
      <br />
      REGON: 091114490
      <br />
    </InfoText>
    <InfoText>
      <a href="tel:+48525840005">
        <FaPhone />
        tel. 52 584 00 05
      </a>
    </InfoText>
    <InfoText>
      <a href="tel:+48525841221">
        <FaPhone />
        tel. 52 584 12 21
      </a>
    </InfoText>
    <InfoText>
      <a href="mailto:info@makromedia.pl">
        <FaEnvelope />
        biuro@makromedia.pl
      </a>
    </InfoText>
  </StyledAddress>
)

const StyledSection = styled.section`
  margin: 30px auto;
`
const Column = styled(Col)`
  min-height: 400px;
  max-height: 700px;
  height: 50vh;
`

const AddressContent = styled.div`
  height: 100%;
  width: auto;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`

const MapWrapper = styled.div`
  position: relative;
  overflow: initial;
  width: 100%;
  height: 100%;
  margin: 0;
  padding: 0;
  z-index: 1;
`

const StyledAddress = styled.address`
  width: 100%;
  margin: 0 auto 0 5%;
`

const InfoText = styled.p`
  font-size: 1rem;

  svg {
    margin-right: 12px;
  }
`

export default ContactSection
