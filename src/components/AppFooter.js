import React from 'react'
import styled from 'styled-components'
import { Link, graphql, StaticQuery } from 'gatsby'
import { Row, Col } from 'react-styled-flexboxgrid'
import {
  FaFacebookF,
  FaTwitter,
  FaLinkedinIn,
  FaChevronCircleRight,
} from 'react-icons/fa'
import AppContainer from './AppContainer'

const AddressContainer = () => (
  <StyledAddress>
    <p className="bold">Makromedia Sp. z o.o.</p>
    <p>
      ul. Dworcowa 81
      <br />
      85-009 Bydgoszcz
      <br />
      REGON: 091114490
    </p>
  </StyledAddress>
)

const ContactContainer = () => (
  <StyledAddress>
    <p>
      <a href="tel:+48525840005">tel. 52 584 00 05</a>
    </p>
    <p>
      <a href="tel:+48525841221">tel. 52 584 12 21</a>
    </p>
    <p>
      <a href="mailto:info@makromedia.pl">biuro@makromedia.pl</a>
    </p>
  </StyledAddress>
)

const AppFooter = ({ siteTitle }) => (
  <StaticQuery
    query={componentQuery}
    render={props => (
      <StyledFooter className="footer">
        <TopSection>
          <AppContainer>
            <Row>
              <StyledCol xs={12} sm={6} md={3}>
                <Logo
                  src={props.logoFile.publicURL}
                  alt="logo firmy Makromedia"
                />
                <h5>Znajdź nas</h5>
                <SocialLinks>
                  <li>
                    <a href="/">
                      <FaFacebookF />
                    </a>
                  </li>
                  <li>
                    <a href="/">
                      <FaTwitter />
                    </a>
                  </li>
                  <li>
                    <a href="/">
                      <FaLinkedinIn />
                    </a>
                  </li>
                </SocialLinks>
              </StyledCol>
              <StyledCol xs={12} sm={6} md={3}>
                <h4>Firma</h4>
                <AddressContainer />
                <h4>Kontakt</h4>
                <ContactContainer />
              </StyledCol>
              <StyledCol xs={12} sm={6} md={3}>
                <h4>Linki</h4>
                <Navigation>
                  <ul>
                    <li>
                      <Link to="/o-firmie">
                        <FaChevronCircleRight />
                        <span>O firmie</span>
                      </Link>
                    </li>
                    <li>
                      <Link to="/oferta">
                        <FaChevronCircleRight />
                        <span>Oferta</span>
                      </Link>
                    </li>
                    <li>
                      <Link to="/kontakt">
                        <FaChevronCircleRight />
                        <span>Kontakt</span>
                      </Link>
                    </li>
                    <li>
                      <Link to="/praca">
                        <FaChevronCircleRight />
                        <span>Praca</span>
                      </Link>
                    </li>
                  </ul>
                </Navigation>
              </StyledCol>
              <StyledCol xs={12} sm={6} md={3}>
                <h4>Zobacz także</h4>
                <SeeAlsoText>
                  At vero eos et accusamus et iusto odio dignissimos ducimus qui
                  blanditiis praesentium voluptatum deleniti atque corrupti quos
                  dolores et quas molestias excepturi sint occaecati cupiditate
                  non provident, similique sunt in culpa qui officia deserunt
                  mollitia animi, id est laborum et dolorum fuga. Et harum
                  quidem rerum facilis est et expedita distinctio. Nam libero
                  tempore, cum soluta nobis est eligendi optio cumque nihil
                </SeeAlsoText>
              </StyledCol>
            </Row>
          </AppContainer>
        </TopSection>
        <BottomSection>
          <AppContainer>
            <Col sm={12}>
              <CopyrightContainer>
                Copyright © {new Date().getFullYear()} Makromedia Sp. z o.o.
                Wszelkie prawa zastrzeżone.
              </CopyrightContainer>
            </Col>
          </AppContainer>
        </BottomSection>
      </StyledFooter>
    )}
  />
)

const StyledFooter = styled.footer`
  text-align: center;

  h4,
  h5 {
    color: ${({ theme }) => theme.colors.white};
  }

  h4 {
    text-align: left;
    text-transform: uppercase;
    font-family: ${({ theme }) => theme.font.family.primary};
    font-weight: ${({ theme }) => theme.font.weight.bold};
    font-size: 15pt;
    line-height: 20pt;
    border-bottom: 1px solid ${({ theme }) => theme.colors.white};
    margin-bottom: 18px;
    max-width: 100%;

    @media ${({ theme }) => theme.media.tablet} {
      max-width: 260px;
    }
  }

  h5 {
    margin-bottom: 20px;
  }

  p {
    color: ${({ theme }) => theme.colors.white};
    line-height: initial;
  }

  a {
    color: ${({ theme }) => theme.colors.white};
    &:hover {
      color: ${({ theme }) => theme.colors.brand};
    }
  }
`
const TopSection = styled.div`
  background-color: ${({ theme }) => theme.colors.accentLight};
  padding-top: 13px;
  padding: 0 2%;
`

const BottomSection = styled.div`
  background-color: ${({ theme }) => theme.colors.accentMid};
  padding: 20px 0;
`

const Logo = styled.img`
  width: 100%;
  max-width: 300px;
  padding-bottom: 20px;
  margin: auto;
`

const StyledCol = styled(Col)`
  margin-bottom: 13px;
  text-align: left;
  padding: 0 5%;

  &:first-child {
    margin: auto;
    padding: 0;
    text-align: center;
    margin-top: 50px;
  }

  @media ${({ theme }) => theme.media.tablet} {
    margin-bottom: 50px;
    padding: 0 0.5rem;
  }
`

const Navigation = styled.nav`
  li {
    height: 35px;

    a {
      color: ${({ theme }) => theme.colors.white};
      text-transform: uppercase;
      font-family: ${({ theme }) => theme.font.family.primary};
      line-height: 25px;
      display: inline-block;

      &:hover {
        filter: none;
      }

      svg {
        font-size: 25px;
        margin-left: 5px;
        margin-right: 15px;
        display: inline-block;
        vertical-align: middle;
      }
    }
  }
`

const SocialLinks = styled.ul`
  margin-top: 0;

  li {
    display: inline-block;
    float: none;
  }

  a {
    display: block;
    border: 1px solid #e8ecee;
    border-radius: 2px;
    border-width: 2px;
    height: 40px;
    width: 40px;
    font-size: 18px;
    line-height: 40px;
    text-align: center;
    margin-right: 5px;
    color: ${({ theme }) => theme.colors.white};

    &:hover {
      background-color: ${({ theme }) => theme.colors.brand};
      border-color: ${({ theme }) => theme.colors.brand};
      color: ${({ theme }) => theme.colors.white};
      filter: none;
    }
  }
`
const StyledAddress = styled.address`
  font-family: ${({ theme }) => theme.font.family.secondary};
  color: ${({ theme }) => theme.colors.white};
  text-align: left;
  margin-left: 12px;

  .bold {
    font-weight: bold;
  }

  a:hover {
    filter: none;
  }
`

const CopyrightContainer = styled.p`
  font-family: ${({ theme }) => theme.font.family.primary};
  font-weight: ${({ theme }) => theme.font.weight.thin};
  color: #d7d7d7;
  margin: 0;
`

const SeeAlsoText = styled.p`
  margin: 0 12px;
`

const componentQuery = graphql`
  query {
    logoFile: file(relativePath: { eq: "logo_white.png" }) {
      publicURL
    }
  }
`
export default AppFooter
