import React from 'react'
import { Link, graphql } from 'gatsby'
import SEO from '../components/seo'
import styled from 'styled-components'
import GlobalStyle from '../theme/GlobalStyle'
import { FaArrowLeft } from 'react-icons/fa'
import Layout from '../components/Layout'
import AppContainer from '../components/AppContainer'
import AppPageTitle from '../components/AppPageTitle'

export default ({ data }) => {
  const { markdownRemark: job } = data
  return (
    <>
      <GlobalStyle />
      <Layout>
        <SEO
          title={job.frontmatter.title}
          keywords={[`gatsby`, `application`, `react`]}
        />
        <AppContainer>
          <AppPageTitle title={job.frontmatter.title} />
          <BackLink to="/praca">
            <BackIcon />
            Powrót do ofert
          </BackLink>
          <Section>
            <div className="blog-post">
              <div
                className="blog-post-content"
                dangerouslySetInnerHTML={{ __html: job.html }}
              />
            </div>
          </Section>
        </AppContainer>
      </Layout>
    </>
  )
}

const BackLink = styled(Link)`
  display: inline-block;
  padding-top: 10px;
  opacity: 0.4;
  color: ${({ theme }) => theme.colors.black};
  font-family: ${({ theme }) => theme.font.family.secondary};
  font-size: 18px;

  :hover {
    opacity: 0.8;
  }
`
const BackIcon = styled(FaArrowLeft)`
  display: inline-block;
  vertical-align: middle;
  font-size: 15px;
  margin-right: 9px;
`

const Section = styled.section`
  margin: 30px auto 40px;

  li {
    list-style: disc;
    margin-inline-start: 20px;
    line-height: 150%;
  }
`

export const pageQuery = graphql`
  query($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      fields {
        slug
      }
      frontmatter {
        title
      }
    }
  }
`
