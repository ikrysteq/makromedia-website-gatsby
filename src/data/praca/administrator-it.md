---
isActive: true
title: 'Administrator sieci i systemów komputerowych'
description: 'Poszukiwana osoba na kontrakt 6-miesięczny.
  UoP 5000-9000 brutto'
---

<h4>Oczekiwania:</h4>

- Znajomość technologii sieciowych z zakresu budowy sieci lokalnych, kampusowych i rozległych
- Znajomość protokołów routingu EIGRP,OSPF i BGP4
- Znajomość co najmniej jednego rozwiązania Firewall/IPS
- Doświadczenie w diagnozowaniu i rozwiązywaniu problemów sieciowych z zakresu sieci LAN,WAN i firewall
- Znajomość języka angielskiego w stopniu umożliwiającym posługiwanie się dokumentacją techniczną
- Chęć poszerzenia wiedzy o rozwiązania techniczne z zakresu: storage, wirtualizacja, backup, Cloud Computing, SDN
- Umiejętność pracy w zespole
- Skuteczna realizacja postawionych celów oraz umiejętności aktywnego poszukiwania rozwiązań

<h4>Dodatkowym atutem będzie:</h4>

- Znajomość Firewall Cisco ASA lub Juniper SRX
- Znajomość rozwiązań LAN/WAN Cisco lub Juniper
- Znajomość rozwiązań wirtualizacji serwerów Vmware lub KVM
- Znajomość dowolnego z systemów automatyzacji konfiguracji

<h4>Obowiązki:</h4>

- Praca w Zespole Administratorów Data Center
- Rozwiązywanie bieżących problemów technicznych
- Projektowanie nowych rozwiązań z zakresu szeroko pojętego Data Center
- Instalacja i konfiguracja sprzętu i oprogramowania
- Bezpośrednie wsparcie techniczne klientów z zakresie łączności z Data Center
- Przygotowywanie dokumentacji technicznej, procedur itp.

<h4>Oferujemy:</h4>

- Pracę w dynamicznie rozwijającej się firmie, w gronie doświadczonych koleżanek i kolegów
- Stabilne zatrudnienie w formie umowy o pracę
- Uczestnictwo w ciekawych projektach informatycznych dla dużych klientów
- Przyjazną atmosferę pracy w otwartym i kreatywnym zespole
- Wykorzystywanie w praktyce najnowszych technologii
- Dodatkowe benefity: karta OKsystem, Sodexo, prywatna opieka medyczna.

<br/>
CV prosimy przesyłać na adres: <a href="mailto:praca@makromedia.pl">praca@makromedia.pl</a>
