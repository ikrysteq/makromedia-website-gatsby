---
isActive: true
title: 'UX Designer'
description: 'Miejsce pracy: Bydgoszcz'
---

Jako UX Designer będziesz prowadzić badania w celu poznania oraz zrozumienia potrzeb i problemów użytkowników, tworzyć persony, przygotowywać koncepcje i makiety, a także projektować i testować interaktywne prototypy aplikacji mobilnych i webowych (RWD). Razem z innymi projektantami, analitykami i programistami w agilnym środowisku będziesz współtworzyć rozwiązania nie tylko dla klientów końcowych, ale też aplikacje biznesowe dla użytkowników korporacyjnych.

<h4>Profil idealnego kandydata:</h4>

- minimum 2-letnie doświadczenie w projektowaniu user experience;
- dobra znajomość technik i metod związanych z procesem projektowym (tworzeniu person, scenariuszy użytkownika, mapowaniu customer journey, testowaniu użyteczności);
- dobra znajomość narzędzi wykorzystywanych przy projektowaniu: Sketch, Axure, UXPin, InVision;
- dobra komunikacja i umiejętność uzasadnienia swoich decyzji projektowych;
- bardzo dobra znajomość języka angielskiego i przynajmniej podstawowa znajomość języka niemieckiego (Capgemini będzie wspierać dalsze kształcenie w zakresie języka niemieckiego w ramach intensywnego kursu języka niemieckiego, bez wpływu na finansowe warunki zatrudnienia).

<h4>Oferujemy:</h4>

- szkolenia zawodowe i językowe (w tym intensywny kurs języka niemieckiego – więcej informacji znajdziesz tutaj);
- udział w prestiżowych projektach międzynarodowych;
- benefity w ramach pakietu socjalnego, m.in. prywatna opieka medyczna, pakiet ubezpieczeniowy, karta Multisport, dofinansowanie zajęć sportowych;
- elastyczny czas pracy;
- wsparcie w relokacji;
- dużą liczbę realizowanych projektów, która umożliwia ich ewentualną zmianę;
- bezpłatne miejsca parkingowe;
- umowę o pracę na czas nieokreślony.

<br/>
CV prosimy przesyłać na adres: <a href="mailto:praca@makromedia.pl">praca@makromedia.pl</a>
