import { createGlobalStyle } from 'styled-components'
import { theme } from '../utils/theme'

export default createGlobalStyle`

  *, *::before, *::after {
    box-sizing: border-box;
  }

  html {
    overflow-x: hidden;
  }

  body {
    padding:0;
    margin:0;
    font-family: ${theme.font.family.primary};
    font-size: ${theme.font.size.base}px;
    font-weight:${theme.font.weight.regular};
    padding-right: 0!important; // for sweetalert2
    overflow-x: hidden;
    color: ${theme.colors.fontColor};
  }

  html.swal2-shown,
  body.swal2-shown {
    overflow-y: auto !important;
  }

  ul {
    padding:0;
    margin:0;
  }

  li {
    list-style: none;
  }

  h1,h2,h3,h4,h5{
    color: ${theme.colors.base};
    margin-top: 20px;
    margin-bottom: 10px;
    line-height: 1.1;
  }

  h1 {
    font-size: 65px;
  }

  h2 {
    font-size: 40px;
  }

  h3 {
    font-size: 28px;
    font-weight:${theme.font.weight.light};
  }

  h4 {
    font-size: 22px;
    font-weight: ${theme.font.weight.regular};
  }

  h5 {
    font-size: 14px;
    text-transform: uppercase;
    font-weight: ${theme.font.weight.bold};
  }

  p {
    font-size: ${theme.font.size.base}px;
    line-height: ${theme.font.size.base * 1.6}px;
    margin: 0 0 10px;
  }

  a {
    text-decoration: none;
    color: ${theme.colors.accentMid};
  }

  a:hover,
  a:focus {
    transition: background-color, color, 0.3s;
    filter: brightness(0.5)
  }

  address {
    font-family: ${theme.font.family.secondary};
    font-style: normal;
    margin-bottom: 20px;
    font-style: normal;
    line-height: 1.42857143;
  }
`
